var empty = { }; // empty object
var cat = { name: "Murzik" };   // an instance with one property
console.log("cat: " + cat.name);

// Store the data about Keanu Reeves
var person = { firstName: "Keanu",
                lastName: "Reeves",
                age: 52};
console.log(person);