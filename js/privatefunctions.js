function Tax(income, dependents) {
    this.income = income;
    this.dependents = dependents;

    var mafiaTaxDeduction = 300;    // private variable in function scope

    this.doTaxes = function () {
        return income*0.05 - dependents*500 - mafiaTaxDeduction;
    }
}

var tax = new Tax(50000, 3);
console.log("Your tax is: " + tax.doTaxes());
console.log("Your mafiaTaxDeduction is " + tax.mafiaTaxDeduction); // Undefined