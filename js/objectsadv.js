
// Store the data about Keanu Reeves
var person = {
    "firstName": "Keanu",
    lastName: "Reeves",
    age: 52,

    stopBullets: function () {
        console.log(this.lastName + " says: Stop!");
    },

    phones: {
        home: "000-111-222",
        work: "333-444-555"
    }
};

var homePhone = person.phones.home;
console.log("homePhone: " + homePhone);

console.log(person.address);    // undefined
console.log(person.address.street); // TypeError

