/*
* No var arguments
* No data type in arguments
* No need to declare return type
*    even if a function return a value
*/
function calculateTax(income, dependents) {
    // Do some stuff here
    console.log("income: " + income + " dep: " + dependents);
}

// In JavaScript it's possible
function calculateTaxTrickier(income, dependents, income, dependants) {
    console.log("income: " + income + " dep: " + dependents);
}

calculateTax(1000, 5);
var tax = calculateTax(500, 2);

calculateTaxTrickier(200, 3, 100, 4); // ???

// Declaring and invoking in the same time
(function calculateTaxInline(income, dependents) {
    console.log("income: " + income + " dep: " + dependents);
})(777, 10);