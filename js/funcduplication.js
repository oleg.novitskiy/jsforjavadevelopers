function Person(name, title) {
    this.name = name;
    this.title = title;
    this.subordinates = [];

    // Duplicates in each instance
    // this.addSubordinates = function (person) {
    //     this.subordinates.push(person);
    // }
}

// No duplication
Person.prototype.addSubordinates = function (person) {
    this.subordinates.push(person);
};

var person1 = new Person("Bill", "Gates");
var person2 = new Person("Tom", "Hanks");
var person3 = new Person("Paul", "Alen");

person3.addSubordinates(person1);
console.log(person3);