function Person(name, title) {
    this.name = name;
    this.title = title;
    this.subordinates = [1,2,3];
}

function Employee(name, title) {
    this.name = name;
    this.title = title;
}

Employee.prototype = new Person();

var employee = new Employee("Alex", "Pupkin");
console.log(employee);